package lecture5.objects;

import java.util.Comparator;

public class CompareTitle implements Comparator<Book>{

    @Override
    public int compare(Book arg0, Book arg1) {
        return arg0.title.compareTo(arg1.title);
    }
}
