package lecture5.objects;

public class Book implements Comparable<Book>{

    String title;
    String author;

    public Book(String title, String author){
        this.title = title;
        this.author = author;        
    }

    public String toString(){
        return String.format("%s - %s", this.title, this.author);
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }

        if(obj instanceof Book){
            Book toCompare = (Book)obj;
            if(this.author.equals(toCompare.author) && this.title.equals(toCompare.title)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int compareTo(Book arg0) {
        int cmp =  this.author.compareTo(arg0.author);
        if(cmp == 0){
            return this.title.compareTo(arg0.title);
        }
        else{
            return cmp;
        }
    }    
}