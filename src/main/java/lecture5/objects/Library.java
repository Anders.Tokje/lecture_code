package lecture5.objects;

import java.util.ArrayList;
import java.util.Collections;

public class Library {
    
    ArrayList<Book> books;

    public Library(){

        books = new ArrayList<Book>();

    }

    void add(Book book){
        books.add(book);

    }

    boolean hasBook(Book book){
        return books.contains(book);
    }

    public static void main(String[] args){

        Library myBooks = new Library();
        System.out.println(myBooks.books);

        myBooks.add(new Book("Hvordan være tøff", "Anders"));

        System.out.println(myBooks.books);

        Book myFavorite = new Book("INF101", "Sondre");

        System.out.println(myBooks.hasBook(myFavorite));

        myBooks.add(myFavorite);

        System.out.println(myBooks.hasBook(myFavorite));

        System.out.println(myBooks.books);

        Book wanted = new Book("Hvordan være tøff", "Anders");
        System.out.println(myBooks.hasBook(wanted));

        myBooks.add(new Book("Dyr", "Tommy"));
        myBooks.add(new Book("Klima", "Susanne"));
        myBooks.add(new Book("Biler", "Ida"));
        myBooks.add(new Book("Andre ting", "Anders"));


        Collections.sort(myBooks.books);
        System.out.println(myBooks.books);

        Collections.sort(myBooks.books, new CompareTitle());
        System.out.println(myBooks.books);
    }

    
}
